package com.example.reservate.Activities.UI.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.reservate.Activities.UI.Bienvenida.PrincipalActivity;
import com.example.reservate.Activities.UI.Details.DetailAyuda;
import com.example.reservate.Activities.UI.Details.DetailSucursales;
import com.example.reservate.Activities.UI.Home.MainActivity;
import com.example.reservate.Adapters.Sucursales.SucursalesAdapter;
import com.example.reservate.Interfaces.GetDataServiceSucursales;
import com.example.reservate.Models.Sucursal;
import com.example.reservate.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SalonesFragment extends Fragment implements MainActivity.OnBackPressedListener
{
    private RecyclerView mRecyclerView;
    List<Sucursal> List;
    private Call<List<Sucursal>> call;
    SucursalesAdapter mAdapter;
    private Toolbar Toolbar;
    View v;
    SwipeRefreshLayout RefreshLayoutSalones;
    private static ConnectivityManager manager;

    private static final String BASE="http://reservate2.pythonanywhere.com/API_Reservate/";

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.recyclerView);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        RefreshLayoutSalones=view.findViewById(R.id.RefreshLayoutSalones);

        GetAllSucursales();

        RefreshLayoutSalones.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetAllSucursales();

                mAdapter.notifyDataSetChanged();
                RefreshLayoutSalones.setRefreshing(false);
            }
        });


        ((MainActivity) getActivity()).setOnBackPressedListener(this);

    }


    public static boolean isOnline(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    public void GetAllSucursales()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GetDataServiceSucursales service= retrofit.create(GetDataServiceSucursales.class);

        call = service.GetAllSucursales();


        if (isOnline(getActivity()))
        {
            call.enqueue(new Callback<List<Sucursal>>() {
                @Override
                public void onResponse(Call<List<Sucursal>> call, Response<List<Sucursal>> response) {

                    if (response.isSuccessful())
                    {

                        List = response.body();

                        mAdapter= new SucursalesAdapter(List,getActivity());

                        mRecyclerView.setAdapter(mAdapter);


                    }
                    else
                    {
                        switch (response.code()) {
                            case 404:
                                ShowErrorDialog();
                                break;
                            case 500:
                                ShowErrorDialog();
                                break;
                            default:
                                ShowErrorDialog();
                                break;
                        }
                    }

                }

                @Override
                public void onFailure(Call<List<Sucursal>> call, Throwable t) {

                }

            });

        }
        else {
           ShowWarningDialog();
        }

    }

    public void ShowErrorDialog()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(getActivity(),R.style.AlertDialogTheme);
         v=LayoutInflater.from(getActivity()).inflate(R.layout.dialog_error,(ConstraintLayout) v.findViewById(R.id.layoutDialogContainer) );

         builder.setView(v);
        ((TextView)v.findViewById(R.id.titulo_mensaje_error)).setText("ERROR");

        ((TextView)v.findViewById(R.id.detalle_mensaje_error)).setText("Existe un problema para procesar su solicitud, Intentelo nuevamente");

        ((Button)v.findViewById(R.id.button_error)).setText("CONTINUAR");

        ((ImageView)v.findViewById(R.id.icono_error)).setImageResource(R.drawable.ic_message_error_24dp);

        final AlertDialog alert=builder.create();

        v.findViewById(R.id.button_error).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        if(alert.getWindow() != null)
        {
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        alert.show();


    }

    public void ShowWarningDialog()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(getActivity(),R.style.AlertDialogTheme);
        v=LayoutInflater.from(getActivity()).inflate(R.layout.dialog_warning,(ConstraintLayout) v.findViewById(R.id.layoutDialogContainer) );

        builder.setView(v);
        ((TextView)v.findViewById(R.id.titulo_mensaje_warning)).setText("REVISE POR FAVOR");

        ((TextView)v.findViewById(R.id.detalle_mensaje_warning)).setText("Actualmente no tiene conexión a internet");

        ((Button)v.findViewById(R.id.button_warning)).setText("CONTINUAR");

        ((ImageView)v.findViewById(R.id.icono_warning)).setImageResource(R.drawable.ic_message_warning_24dp);

        final AlertDialog alert=builder.create();

        v.findViewById(R.id.button_warning).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                GetAllSucursales();
            }
        });


        if(alert.getWindow() != null)
        {
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        alert.show();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){

        v= inflater.inflate(R.layout.fragment_salones,container,false);

        return v;

    }

    @Override
    public void doBack() {
        getActivity().finish();
    }
}
