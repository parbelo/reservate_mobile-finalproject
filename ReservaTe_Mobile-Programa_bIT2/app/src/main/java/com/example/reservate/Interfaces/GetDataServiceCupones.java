package com.example.reservate.Interfaces;

import com.example.reservate.Models.Cupon;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataServiceCupones {

    @GET("Cupones/")
    Call<List<Cupon>> GetAllCupones();

}