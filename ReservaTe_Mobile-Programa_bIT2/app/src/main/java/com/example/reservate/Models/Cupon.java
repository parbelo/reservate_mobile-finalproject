package com.example.reservate.Models;

import com.google.gson.annotations.SerializedName;

public class Cupon {

    @SerializedName("id_Cupon")
    private int id_Cupon;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("descripcion")
    private String descripcion;
    @SerializedName("descuento")
    private int descuento;
    @SerializedName("validez")
    private String validez;
    @SerializedName("aplicado")
    private boolean aplicado;
    @SerializedName("codigo")
    private String codigo;
    @SerializedName("foto")
    private String foto;

    public Cupon(int id_Cupon, String nombre, String descripcion, int descuento, String validez, boolean aplicado, String codigo, String foto) {
        this.id_Cupon = id_Cupon;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.descuento = descuento;
        this.validez = validez;
        this.aplicado = aplicado;
        this.codigo = codigo;
        this.foto = foto;
    }

    public int getId_Cupon() {
        return id_Cupon;
    }

    public void setId_Cupon(int id_Cupon) {
        this.id_Cupon = id_Cupon;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public String getValidez() {
        return validez;
    }

    public void setValidez(String validez) {
        this.validez = validez;
    }

    public boolean isAplicado() {
        return aplicado;
    }

    public void setAplicado(boolean aplicado) {
        this.aplicado = aplicado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
