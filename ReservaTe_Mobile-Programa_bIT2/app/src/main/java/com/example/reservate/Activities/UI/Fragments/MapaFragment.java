package com.example.reservate.Activities.UI.Fragments;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.example.reservate.Activities.UI.Home.MainActivity;
import com.example.reservate.Adapters.Sucursales.SucursalesAdapter;
import com.example.reservate.Interfaces.GetDataServiceSucursales;
import com.example.reservate.Models.Sucursal;
import com.example.reservate.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapaFragment extends Fragment implements MainActivity.OnBackPressedListener{

    public List<Sucursal> List=new ArrayList<Sucursal>();
    private Call<List<Sucursal>> call;
    SucursalesAdapter mAdapter;

    private List<Sucursal> list_ubi=new ArrayList<Sucursal>();

    private ArrayList<Marker> MarkersArray=new ArrayList<Marker>();

    GoogleMap mapApI;

    SupportMapFragment mapFragment;

    String toolTip;

    View v;

    private static final String BASE="http://reservate2.pythonanywhere.com/API_Reservate/";

    public void ShowErrorDialog()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(getActivity(),R.style.AlertDialogTheme);
        v=LayoutInflater.from(getActivity()).inflate(R.layout.dialog_error,(ConstraintLayout) v.findViewById(R.id.layoutDialogContainer) );

        builder.setView(v);
        ((TextView)v.findViewById(R.id.titulo_mensaje_error)).setText("ERROR");

        ((TextView)v.findViewById(R.id.detalle_mensaje_error)).setText("Existe un problema para procesar su solicitud, Intentelo nuevamente");

        ((Button)v.findViewById(R.id.button_error)).setText("CONTINUAR");

        ((ImageView)v.findViewById(R.id.icono_error)).setImageResource(R.drawable.ic_message_error_24dp);

        final AlertDialog alert=builder.create();

        v.findViewById(R.id.button_error).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        if(alert.getWindow() != null)
        {
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        alert.show();


    }

    public void ShowWarningDialog()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(getActivity(),R.style.AlertDialogTheme);
        v=LayoutInflater.from(getActivity()).inflate(R.layout.dialog_warning,(ConstraintLayout) v.findViewById(R.id.layoutDialogContainer) );

        builder.setView(v);
        ((TextView)v.findViewById(R.id.titulo_mensaje_warning)).setText("REVISE POR FAVOR");

        ((TextView)v.findViewById(R.id.detalle_mensaje_warning)).setText("Actualmente no tiene conexión a internet");

        ((Button)v.findViewById(R.id.button_warning)).setText("CONTINUAR");

        ((ImageView)v.findViewById(R.id.icono_warning)).setImageResource(R.drawable.ic_message_warning_24dp);

        final AlertDialog alert=builder.create();

        v.findViewById(R.id.button_warning).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                GetAllSucursales();
            }
        });


        if(alert.getWindow() != null)
        {
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        alert.show();

    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        ((MainActivity) getActivity()).setOnBackPressedListener(this);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapAPI_Ubi);

        GetAllSucursales();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v= inflater.inflate(R.layout.fragment_mapa,container,false);

        return v;

    }

    public void GetAllSucursales() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GetDataServiceSucursales service= retrofit.create(GetDataServiceSucursales.class);

        call = service.GetAllSucursales();

        call.enqueue(new Callback<List<Sucursal>>() {
            @Override
            public void onResponse(Call<List<Sucursal>> call, Response<List<Sucursal>> response) {

                if (response.isSuccessful())
                {
                    System.out.println(response.body());

                    List = response.body();

                    mapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {

                            mapApI = googleMap;


                            for (int i = 0; i < List.size(); i++) {

                                LatLng asdads = new LatLng(List.get(i).getLatitud(),List.get(i).getLongitud());
                                //LatLng asdads=new LatLng(-34.880705,-56.134705);

                                toolTip = List.get(i).getNombre()+" - "+List.get(i).getDireccion();

                                Marker marker = mapApI.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_iconmap)).anchor(0.0f, 1.0f).position(asdads).title(toolTip));
                                float zoomLevel = (float) 12.0;
                                mapApI.moveCamera(CameraUpdateFactory.newLatLngZoom(asdads, zoomLevel));
                                mapApI.getUiSettings().setZoomControlsEnabled(true);

                                MarkersArray.add(marker);

                            }
                            mapApI.setMyLocationEnabled(true);

                        }
                    });

                }
                else
                {
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(getActivity(), "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(getActivity(), "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<List<Sucursal>> call, Throwable t) {

                ShowWarningDialog();
            }
        });
    }

    @Override
    public void doBack() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container,new SalonesFragment()).addToBackStack("a").commit();
    }

}