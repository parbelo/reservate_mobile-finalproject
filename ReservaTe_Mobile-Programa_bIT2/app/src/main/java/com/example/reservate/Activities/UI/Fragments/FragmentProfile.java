package com.example.reservate.Activities.UI.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.reservate.Activities.UI.Account.LoginActivity;
import com.example.reservate.Activities.UI.Bienvenida.PrincipalActivity;
import com.example.reservate.Activities.UI.Home.MainActivity;
import com.example.reservate.Adapters.Sucursales.SucursalesAdapter;
import com.example.reservate.Interfaces.GetDataServiceSucursales;
import com.example.reservate.Interfaces.GetDateServiceUsers;
import com.example.reservate.Models.Sucursal;
import com.example.reservate.Models.User;
import com.example.reservate.R;
import com.example.reservate.Storage.SharedPrefManager;

import java.util.List;

public class FragmentProfile extends Fragment implements View.OnClickListener, MainActivity.OnBackPressedListener {

    private TextView TxtUsername,TxtFullName,TxtEmail;


    private Button Logout;

    private static final String BASE="http://reservate2.pythonanywhere.com/API_Reservate/";

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TxtEmail = view.findViewById(R.id.email_profile);
        TxtUsername = view.findViewById(R.id.username_field);
        TxtFullName = view.findViewById(R.id.full_name_profile);
        Logout = view.findViewById(R.id.btn_logout);

        TxtEmail.setText(SharedPrefManager.getInstance(getActivity()).getUser().getEmail());
        TxtUsername.setText(SharedPrefManager.getInstance(getActivity()).getUser().getUsername());
        TxtFullName.setText(SharedPrefManager.getInstance(getActivity()).getUser().getFullName());

        Logout.setOnClickListener(this);

        ((MainActivity) getActivity()).setOnBackPressedListener(this);
    }

    private void Logout()
    {
        User u=SharedPrefManager.getInstance(getActivity()).getUser();
        SharedPrefManager.getInstance(getActivity()).clear();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_logout:
                Logout();
                break;
        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View v= inflater.inflate(R.layout.fragment_profile,container,false);

        return v;
    }

    @Override
    public void doBack() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container,new SalonesFragment()).addToBackStack("a").commit();
    }


}
