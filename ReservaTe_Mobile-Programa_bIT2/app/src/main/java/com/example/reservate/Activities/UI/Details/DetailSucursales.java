package com.example.reservate.Activities.UI.Details;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.reservate.Activities.UI.Fragments.SalonesFragment;
import com.example.reservate.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

public class DetailSucursales extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap mapApI;

    SupportMapFragment mapFragment;

    private TextView nombre,direccion;

    private ImageView imagen;

    private Float Latitud,Longitud;
    private String NombreMap;

    String Nombre;

    private Toolbar toolbar;

    private final int REQUEST_ACCESS_FINE = 0;

    public void ShowWarningDialog()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(this,R.style.AlertDialogTheme);
        View v= LayoutInflater.from(this).inflate(R.layout.dialog_warning,(ConstraintLayout)findViewById(R.id.layoutDialogContainer) );

        builder.setView(v);
        ((TextView)findViewById(R.id.titulo_mensaje_warning)).setText("REVISE POR FAVOR");

        ((TextView)findViewById(R.id.detalle_mensaje_warning)).setText("Actualmente no tiene conexión a internet");

        ((Button)findViewById(R.id.button_warning)).setText("CONTINUAR");

        ((ImageView)findViewById(R.id.icono_warning)).setImageResource(R.drawable.ic_message_warning_24dp);

        final AlertDialog alert=builder.create();

        v.findViewById(R.id.button_warning).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });


        if(alert.getWindow() != null)
        {
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        alert.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_detail);


        imagen=findViewById(R.id.barber_foto);
        nombre=findViewById(R.id.barber_nombre);
        direccion=findViewById(R.id.barber_direccion);

        String Imagen=getIntent().getExtras().getString("barber_foto");

        Glide.with(this).load(Imagen).into(imagen);

         Nombre=getIntent().getExtras().getString("barber_nombre");

        if(Nombre != null || Nombre.isEmpty()) {

            nombre.setText(Nombre);

            NombreMap = getIntent().getExtras().getString("barber_nombre");


            String Direccion = getIntent().getExtras().getString("barber_direccion");

            direccion.setText(Direccion);

            Latitud = getIntent().getExtras().getFloat("latitud");

            Longitud = getIntent().getExtras().getFloat("longitud");
        }
        else
        {
            ShowWarningDialog();

            nombre.setText(Nombre);

            NombreMap = getIntent().getExtras().getString("barber_nombre");


            String Direccion = getIntent().getExtras().getString("barber_direccion");

            direccion.setText(Direccion);

            Latitud = getIntent().getExtras().getFloat("latitud");

            Longitud = getIntent().getExtras().getFloat("longitud");
        }

       mapFragment=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.mapAPI);

       mapFragment.getMapAsync(this);

        setUpToolbar();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String [] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_ACCESS_FINE) {
            if(grantResults.length > 0 && grantResults [0] == PackageManager.PERMISSION_GRANTED)
                Toast.makeText(this, "Permiso concedido", Toast.LENGTH_SHORT).show();

            else
                Toast.makeText(this, "Permiso denegado", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapApI=googleMap;


        LatLng asdads=new LatLng(Latitud,Longitud);

        mapApI.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_iconmap)).anchor(0.0f,1.0f).position(asdads).title(NombreMap));
        float zoomLevel = (float) 16.0;
        mapApI.moveCamera(CameraUpdateFactory.newLatLngZoom(asdads, zoomLevel));

        mapApI.getUiSettings().setZoomControlsEnabled(true);

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            return;
        }
        mapApI.setMyLocationEnabled(true);
    }



    private void setUpToolbar()
    {
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        showHomeUpIcon();
        customTittleToolbar();
    }

    public void customTittleToolbar()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            TextView title=toolbar.findViewById(R.id.toolbar_title);

            title.setText(Nombre);
        }
    }

    public void showHomeUpIcon()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
