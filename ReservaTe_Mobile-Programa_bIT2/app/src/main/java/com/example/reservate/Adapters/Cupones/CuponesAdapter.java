package com.example.reservate.Adapters.Cupones;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.reservate.Activities.UI.Details.DetailCupones;
import com.example.reservate.Activities.UI.Details.DetailSucursales;
import com.example.reservate.Models.Cupon;
import com.example.reservate.Models.Sucursal;
import com.example.reservate.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CuponesAdapter extends RecyclerView.Adapter<CuponesAdapter.CuponesViewHolder> {

    private List<Cupon> List;

    private Context context;


    public CuponesAdapter(List<Cupon> list, Context context) {
        this.List = list;
        this.context=context;
    }

    @NonNull
    @Override
    public CuponesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View mView=LayoutInflater.from(context).inflate(R.layout.cupones_item,parent, false);

        return new CuponesViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull CuponesViewHolder holder, int position) {

        Cupon currentItem = List.get(position);

        holder.nombre_servicio.setText(currentItem.getNombre());
        holder.detalle_cupon.setText(currentItem.getDescripcion());

        LoadImage(holder.img_servicio,currentItem.getFoto());

    }

    private void LoadImage(final ImageView imageView,final String URL)
    {

        Glide.with(context).load(URL).diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(imageView);

    }

    @Override
    public int getItemCount() {
            return  List.size();
    }


    public class CuponesViewHolder extends RecyclerView.ViewHolder{

        public ImageView img_servicio;
        public TextView nombre_servicio;
        public TextView detalle_cupon;
        public Button Obtener_Cupon;


        public CuponesViewHolder( View itemView) {
            super(itemView);

            img_servicio = itemView.findViewById(R.id.img_servicio);
            nombre_servicio = itemView.findViewById(R.id.nombre_servicio);
            detalle_cupon = itemView.findViewById(R.id.detalle_cupon);

            Obtener_Cupon=itemView.findViewById(R.id.boton_obtener_cupon);

            Obtener_Cupon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos=getAdapterPosition();
                    Cupon Detail=List.get(pos);
                    Intent intent=new Intent(context, DetailCupones.class);

                    String Descuento=String.valueOf(List.get(pos).getDescuento()) + " % " + " Dto";

                    intent.putExtra("detalle_servicio",List.get(pos).getNombre());
                    intent.putExtra("detalle_imagen",List.get(pos).getFoto());
                    intent.putExtra("detalle_descuento",Descuento);
                    intent.putExtra("detalle_codigo",List.get(pos).getCodigo());

                    intent.putExtra("detalle_cupon",List.get(pos).getDescripcion());

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(intent);


                }
            });
        }
    }
}
