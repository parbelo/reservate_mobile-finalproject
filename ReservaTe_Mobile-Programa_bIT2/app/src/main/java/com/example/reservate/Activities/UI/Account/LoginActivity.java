package com.example.reservate.Activities.UI.Account;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.reservate.Activities.UI.Home.MainActivity;
import com.example.reservate.Interfaces.GetDataServiceSucursales;
import com.example.reservate.Interfaces.GetDateServiceUsers;
import com.example.reservate.Models.Sucursal;
import com.example.reservate.Models.User;
import com.example.reservate.Network.RetrofitClientInstance;
import com.example.reservate.R;
import com.example.reservate.Storage.SharedPrefManager;

import java.io.IOException;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText TxtUsername;
    private EditText TxtPassword;

    private static final String BASE="http://reservate2.pythonanywhere.com/API_Reservate/";

    private Call<User> call;

    private Button loguearse;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TxtUsername=findViewById(R.id.user);
        TxtPassword=findViewById(R.id.password);

        loguearse=findViewById(R.id.login1);

        loguearse.setOnClickListener(LoginActivity.this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.login1:
                UserLogin();
                break;
        }
    }

    public void UserLogin()
    {
        String username=TxtUsername.getText().toString().trim();
        String password=TxtPassword.getText().toString().trim();

        if(username.isEmpty())
        {
            TxtUsername.setError("El email es requerido");
            TxtUsername.requestFocus();
            return;
        }

        if(password.isEmpty())
        {
            TxtPassword.setError("La contrasña es requerida");
            TxtPassword.requestFocus();
            return;
        }

        if(password.length() < 7)
        {
            TxtPassword.setError("El email debe tener 8 caracteres como minimo");
            TxtPassword.requestFocus();
            return;
        }

        String base=username + ":" + password;

        String Auth = "Basic " + Base64.encodeToString(base.getBytes(),Base64.NO_WRAP);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GetDateServiceUsers service= retrofit.create(GetDateServiceUsers.class);

        call = service.GetUser(Auth);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if(response.isSuccessful()) {

                    User Body = response.body();

                    SharedPrefManager.getInstance(LoginActivity.this)
                            .saveUser(Body);

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                    Toast.makeText(LoginActivity.this,"El usuario fue logueado correctamente",Toast.LENGTH_SHORT).show();

                }
                else
                {
                 Toast.makeText(LoginActivity.this,response.message(),Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });




    }
}