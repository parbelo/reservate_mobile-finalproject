package com.example.reservate.Adapters.Help;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.example.reservate.Models.PregFrec_PrivaCond;
import com.example.reservate.R;

import java.util.List;

public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.HelpViewHolder> {

    private List<PregFrec_PrivaCond> List;

    private Context context;

    public HelpAdapter(List<PregFrec_PrivaCond> list, Context context) {
        this.List = list;
        this.context=context;
    }

    @NonNull
    @Override
    public HelpViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View mView=LayoutInflater.from(context).inflate(R.layout.faq_item,parent, false);

        return new HelpViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull HelpViewHolder holder, int position) {

        PregFrec_PrivaCond currentItem = List.get(position);

        holder.txt_nombrepregunta.setText(currentItem.getNombre());
        holder.detalle_pregunta.setText(currentItem.getDescripcion());

    }

    @Override
    public int getItemCount() {
            return  List.size();
    }


    public class HelpViewHolder extends RecyclerView.ViewHolder{

        public TextView txt_nombrepregunta;
        public TextView detalle_pregunta;


        public HelpViewHolder( View itemView) {
            super(itemView);

            txt_nombrepregunta = itemView.findViewById(R.id.txt_preguntanombre);
            detalle_pregunta = itemView.findViewById(R.id.txt_pregunta);


        }
    }
}
