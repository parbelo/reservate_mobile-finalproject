package com.example.reservate.Activities.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.reservate.Activities.UI.Account.LoginActivity;
import com.example.reservate.Activities.UI.Details.DetailAyuda;
import com.example.reservate.Activities.UI.Details.Detail_AppInfo;
import com.example.reservate.Activities.UI.Details.Detail_Condiciones;
import com.example.reservate.Activities.UI.Home.MainActivity;
import com.example.reservate.R;

public class FramentHelp extends Fragment implements View.OnClickListener, MainActivity.OnBackPressedListener {

    TextView Faq,Info,Private;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_help, container, false);

        Faq = (TextView) view.findViewById(R.id.txt_faq);
        Faq.setOnClickListener(this);

        Info = (TextView) view.findViewById(R.id.txt_info);
        Info.setOnClickListener(this);

        Private = (TextView) view.findViewById(R.id.txt_private);
        Private.setOnClickListener(this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity) getActivity()).setOnBackPressedListener(this);

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_faq:
            Intent intent = new Intent(getActivity(), DetailAyuda.class);
            startActivity(intent);
            break;
            case R.id.txt_private:
                Intent intent2 = new Intent(getActivity(), Detail_Condiciones.class);
                startActivity(intent2);
                break;
            case R.id.txt_info:
                Intent intent3 = new Intent(getActivity(), Detail_AppInfo.class);
                startActivity(intent3);
                break;
        }
    }

    @Override
    public void doBack() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container,new SalonesFragment()).addToBackStack("a").commit();
    }

}
