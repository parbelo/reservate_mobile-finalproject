package com.example.reservate.Activities.UI.Details;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.reservate.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class DetailCupones extends AppCompatActivity {

    TextView detalle_servicio,detalle_codigo,detalle_descuento,desc_cupon;

    ImageView detalle_imagen;
    ImageView ivQRcode;
    String Detalle_Servicio;

    private Toolbar toolbar;

    public void ShowWarningDialog()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(this,R.style.AlertDialogTheme);
        View v=LayoutInflater.from(this).inflate(R.layout.dialog_warning,(ConstraintLayout)findViewById(R.id.layoutDialogContainer) );

        builder.setView(v);
        ((TextView)findViewById(R.id.titulo_mensaje_warning)).setText("REVISE POR FAVOR");

        ((TextView)findViewById(R.id.detalle_mensaje_warning)).setText("Actualmente no tiene conexión a internet");

        ((Button)findViewById(R.id.button_warning)).setText("CONTINUAR");

        ((ImageView)findViewById(R.id.icono_warning)).setImageResource(R.drawable.ic_message_warning_24dp);

        final AlertDialog alert=builder.create();

        findViewById(R.id.button_warning).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });


        if(alert.getWindow() != null)
        {
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        alert.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_cupon);

        detalle_imagen=findViewById(R.id.cupon_imagen);
        //detalle_servicio=findViewById(R.id.cupon_servicio);
        detalle_descuento=findViewById(R.id.cupon_descuento);
        detalle_codigo=findViewById(R.id.codigo_QR);

        desc_cupon=findViewById(R.id.cupon_detalle);

        ivQRcode = (ImageView) findViewById(R.id.cupon_imageQR);

        Detalle_Servicio=getIntent().getExtras().getString("detalle_servicio");

        //detalle_servicio.setText(Detalle_Servicio);

        String Detalle_Imagen=getIntent().getExtras().getString("detalle_imagen");

        Glide.with(this).load(Detalle_Imagen).into(detalle_imagen);

        String Detalle_Descuento=getIntent().getExtras().getString("detalle_descuento");

        if(Detalle_Descuento != null || Detalle_Descuento.isEmpty()) {

            detalle_descuento.setText(Detalle_Descuento);

            String Desc_Cupon = getIntent().getExtras().getString("detalle_cupon");

            desc_cupon.setText(Desc_Cupon);

            String Detalle_Codigo = getIntent().getExtras().getString("detalle_codigo");

            detalle_codigo.setText(Detalle_Codigo);
        }
        else
        {
            ShowWarningDialog();

        }

        generarQRcode();

        setUpToolbar();

    }

    private void generarQRcode() {
        String textCod = detalle_codigo.getText().toString();
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(textCod, BarcodeFormat.QR_CODE, 400,400);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            ivQRcode.setImageBitmap(bitmap);
        }catch (WriterException e){
            e.printStackTrace();
        }


}

    private void setUpToolbar()
    {
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        showHomeUpIcon();
        customTittleToolbar();
    }

    public void customTittleToolbar()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            TextView title=toolbar.findViewById(R.id.toolbar_title);

            title.setText(Detalle_Servicio);
        }
    }

    public void showHomeUpIcon()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
