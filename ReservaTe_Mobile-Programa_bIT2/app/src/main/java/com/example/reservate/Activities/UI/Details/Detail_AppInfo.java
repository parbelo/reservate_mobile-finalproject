package com.example.reservate.Activities.UI.Details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.TextView;

import com.example.reservate.R;

public class Detail_AppInfo extends AppCompatActivity {

    public Toolbar toolbar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail__appinfo);

        setUpToolbar();
    }

    public void setUpToolbar()
    {
        toolbar2=findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar2);
        showHomeUpIcon();
        customTittleToolbar();
    }


    public void customTittleToolbar()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            TextView title=toolbar2.findViewById(R.id.toolbar_title);

            title.setText("Información");
        }
    }

    public void showHomeUpIcon()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
