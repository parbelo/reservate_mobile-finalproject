package com.example.reservate.Models;

import com.google.gson.annotations.SerializedName;

public class Sucursal {

    @SerializedName("id")
    private int id;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("direccion")
    private String direccion;
    @SerializedName("longitud")
    private Float  longitud;
    @SerializedName("latitud")
    private Float  latitud;
    @SerializedName("telefono")
    private int telefono;
    @SerializedName("foto")
    private String foto;


    public Sucursal(int id, String nombre, Float latitud, Float longitud) {
        this.id = id;
        this.nombre = nombre;
        this.longitud = longitud;
        this.latitud = latitud;
    }

    public Sucursal(int id, String nombre, String direccion, Float longitud, Float latitud, int telefono, String image) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.longitud = longitud;
        this.latitud = latitud;
        this.telefono = telefono;
        foto = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Float getLongitud() {
        return longitud;
    }

    public void setLongitud(Float longitud) {
        this.longitud = longitud;
    }

    public Float getLatitud() {
        return latitud;
    }

    public void setLatitud(Float latitud) {
        this.latitud = latitud;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getImage() {
        return foto;
    }

    public void setImage(String image) {
        foto = image;
    }
}
