package com.example.reservate.Activities.UI.Details;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.reservate.Adapters.Help.HelpAdapter;
import com.example.reservate.Adapters.Sucursales.SucursalesAdapter;
import com.example.reservate.Interfaces.GetDataServiceHelp;
import com.example.reservate.Interfaces.GetDataServiceSucursales;
import com.example.reservate.Models.PregFrec_PrivaCond;
import com.example.reservate.Models.Sucursal;
import com.example.reservate.R;

import java.util.List;

public class DetailAyuda extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    List<PregFrec_PrivaCond> List;
    private Call<List<PregFrec_PrivaCond>> call;
    HelpAdapter mAdapter;

    private Toolbar toolbar;

    private static final String BASE="http://reservate2.pythonanywhere.com/API_Reservate/";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.faq_activity);


                mRecyclerView = findViewById(R.id.recyclerView_faq);

                mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

                GetAllFaqs();

                setUpToolbar();

            }


    private void setUpToolbar()
    {
        toolbar=findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        showHomeUpIcon();
        customTittleToolbar();
    }

    public void customTittleToolbar()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            TextView title=toolbar.findViewById(R.id.toolbar_title);

            title.setText("Preguntas Frecuentes");
        }
    }

    public void showHomeUpIcon()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void ShowWarningDialog()
    {
        AlertDialog.Builder builder= new AlertDialog.Builder(this,R.style.AlertDialogTheme);
        View v= LayoutInflater.from(this).inflate(R.layout.dialog_warning,(ConstraintLayout)findViewById(R.id.layoutDialogContainer) );

        builder.setView(v);
        ((TextView)v.findViewById(R.id.titulo_mensaje_warning)).setText("REVISE POR FAVOR");

        ((TextView)v.findViewById(R.id.detalle_mensaje_warning)).setText("Actualmente no tiene conexión a internet");

        ((Button)v.findViewById(R.id.button_warning)).setText("CONTINUAR");

        ((ImageView)v.findViewById(R.id.icono_warning)).setImageResource(R.drawable.ic_message_warning_24dp);

        final AlertDialog alert=builder.create();

        v.findViewById(R.id.button_warning).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                GetAllFaqs();
            }
        });


        if(alert.getWindow() != null)
        {
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        alert.show();

    }

    public void GetAllFaqs()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GetDataServiceHelp service= retrofit.create(GetDataServiceHelp.class);

        call = service.GetAllhelp();

        call.enqueue(new Callback<List<PregFrec_PrivaCond>>() {
            @Override
            public void onResponse(Call<List<PregFrec_PrivaCond>> call, Response<List<PregFrec_PrivaCond>> response) {

                if (response.isSuccessful())
                {

                    List = response.body();

                    mAdapter= new HelpAdapter(List,DetailAyuda.this);

                    mRecyclerView.setAdapter(mAdapter);

                }
                else
                {
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(DetailAyuda.this, response.message(), Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(DetailAyuda.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(DetailAyuda.this, "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<List<PregFrec_PrivaCond>> call, Throwable t) {

                ShowWarningDialog();
            }
        });
    }

}
