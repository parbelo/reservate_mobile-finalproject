package com.example.reservate.Activities.UI.Details;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.reservate.R;;

public class Detail_Condiciones extends AppCompatActivity {

    public Toolbar toolbar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail__condiciones);

        setUpToolbar();
    }

    public void setUpToolbar()
    {
        toolbar2=findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar2);
        showHomeUpIcon();
        customTittleToolbar();
    }


    public void customTittleToolbar()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            TextView title=toolbar2.findViewById(R.id.toolbar_title);

            title.setText("Condiciones y Privacidad");
        }
    }

    public void showHomeUpIcon()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
       return true;
    }
}
