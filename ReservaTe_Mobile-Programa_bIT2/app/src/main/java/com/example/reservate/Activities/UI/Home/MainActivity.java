package com.example.reservate.Activities.UI.Home;

import android.os.Bundle;

import androidx.annotation.NonNull;

import com.example.reservate.Activities.UI.Fragments.CuponesFragment;
import com.example.reservate.Activities.UI.Fragments.MapaFragment;
import com.example.reservate.Storage.SharedPrefManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.Menu;
import android.view.MenuItem;

import com.example.reservate.Activities.UI.Fragments.FragmentProfile;
import com.example.reservate.Activities.UI.Fragments.FramentHelp;
import com.example.reservate.Activities.UI.Fragments.SalonesFragment;
import com.example.reservate.R;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);

        menu=navigation.getMenu();

        if(!SharedPrefManager.getInstance(this).isLoggedIn())
        {
            MenuItem item=menu.findItem(R.id.profile);
            item.setVisible(false);
        }


        navigation.setOnNavigationItemSelectedListener(this);

        DisplayFragment(new SalonesFragment());

    }

    private void DisplayFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.salones:
                fragment = new SalonesFragment();
                break;
            case R.id.cupones:
                fragment = new CuponesFragment();
                break;
            case R.id.profile:
                fragment = new FragmentProfile();
                break;
            case R.id.help:
                fragment = new FramentHelp();
                break;
            case R.id.ubicaciones:
                fragment = new MapaFragment();
                break;
        }
        if (fragment != null) {
            DisplayFragment(fragment);
        }

        return false;
    }


    protected OnBackPressedListener onBackPressedListener;

    public interface OnBackPressedListener {
        void doBack();
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    protected void onDestroy() {
        onBackPressedListener = null;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null) {
            onBackPressedListener.doBack();
        } else {
            super.onBackPressed();
        }
    }

}
