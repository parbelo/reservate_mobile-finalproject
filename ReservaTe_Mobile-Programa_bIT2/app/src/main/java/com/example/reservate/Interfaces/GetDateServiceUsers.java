package com.example.reservate.Interfaces;

import com.example.reservate.Models.Sucursal;
import com.example.reservate.Models.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface GetDateServiceUsers {

    @FormUrlEncoded
    @POST("auth/login/")
    Call<ResponseBody> Login(@Field("username") String username,@Field("password") String password);

    @GET("auth/user/")
    Call<User> GetUser(@Header("Authorization") String authheader);
}
