package com.example.reservate.Interfaces;


import com.example.reservate.Models.PregFrec_PrivaCond;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;


public interface GetDataServiceHelp {

    @GET("Faqs/")
    Call<List<PregFrec_PrivaCond>> GetAllhelp();

}