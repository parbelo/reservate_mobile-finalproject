package com.example.reservate.Adapters.Sucursales;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.reservate.Activities.UI.Details.DetailSucursales;
import com.example.reservate.Models.Sucursal;
import com.example.reservate.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SucursalesAdapter extends RecyclerView.Adapter<SucursalesAdapter.SucursalesViewHolder> {

    private List<Sucursal> List;
    private Context context;

    public SucursalesAdapter(List<Sucursal> list,Context context) {
        this.List = list;
        this.context=context;
    }


    @NonNull
    @Override
    public SucursalesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View mView=LayoutInflater.from(context).inflate(R.layout.salones_item,parent, false);

        return new SucursalesViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull SucursalesViewHolder holder, int position) {

        Sucursal currentItem = List.get(position);

        holder.nombre.setText(currentItem.getNombre());

        holder.direccion.setText(currentItem.getDireccion());

        LoadImage(holder.Imagen,currentItem.getImage());

    }

    private void LoadImage(final ImageView imageView,final String URL)
    {

        Glide.with(context).load(URL).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);

    }

    @Override
    public int getItemCount() {
            return  List.size();
    }


    public class SucursalesViewHolder extends RecyclerView.ViewHolder{

        public ImageView Imagen;
        public TextView nombre;
        public TextView direccion;

        public Button Info;


        public SucursalesViewHolder(View itemView) {
                super(itemView);

            Imagen = itemView.findViewById(R.id.image_sucursal);
            nombre = itemView.findViewById(R.id.txt_nombre);
            direccion = itemView.findViewById(R.id.txt_direccion);

            Info=itemView.findViewById(R.id.btn_info);

            Info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos=getAdapterPosition();
                    Sucursal Detail=List.get(pos);
                    Intent intent=new Intent(context, DetailSucursales.class);

                    intent.putExtra("barber_foto",List.get(pos).getImage());
                    intent.putExtra("barber_nombre",List.get(pos).getNombre());
                    intent.putExtra("barber_direccion",List.get(pos).getDireccion());

                    intent.putExtra("latitud",List.get(pos).getLatitud());
                    intent.putExtra("longitud",List.get(pos).getLongitud());

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(intent);




                }
            });


        }
    }
}
