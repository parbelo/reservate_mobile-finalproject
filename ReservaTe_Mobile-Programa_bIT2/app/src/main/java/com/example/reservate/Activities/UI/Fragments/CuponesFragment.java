package com.example.reservate.Activities.UI.Fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.reservate.Activities.UI.Home.MainActivity;
import com.example.reservate.Activities.UI.Home.MainActivity.OnBackPressedListener;
import com.example.reservate.Adapters.Cupones.CuponesAdapter;
import com.example.reservate.Adapters.Sucursales.SucursalesAdapter;
import com.example.reservate.Interfaces.GetDataServiceCupones;
import com.example.reservate.Models.Cupon;

import com.example.reservate.R;

import java.util.List;

public class CuponesFragment extends Fragment implements OnBackPressedListener
{
    private RecyclerView mRecyclerView;
    List<Cupon> List;
    private Call<List<Cupon>> call;
    CuponesAdapter mAdapter;
    private static long presionado;
    View v;


    private static final String BASE="http://reservate2.pythonanywhere.com/API_Reservate/";

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.cupones_recyclerView);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        GetAllCupones();

        ((MainActivity) getActivity()).setOnBackPressedListener(this);

    }


    public void ShowErrorDialog()
    {
        androidx.appcompat.app.AlertDialog.Builder builder= new androidx.appcompat.app.AlertDialog.Builder(getActivity(),R.style.AlertDialogTheme);
        v=LayoutInflater.from(getActivity()).inflate(R.layout.dialog_error,(ConstraintLayout) v.findViewById(R.id.layoutDialogContainer) );

        builder.setView(v);
        ((TextView)v.findViewById(R.id.titulo_mensaje_error)).setText("ERROR");

        ((TextView)v.findViewById(R.id.detalle_mensaje_error)).setText("Existe un problema para procesar su solicitud, Intentelo nuevamente");

        ((Button)v.findViewById(R.id.button_error)).setText("CONTINUAR");

        ((ImageView)v.findViewById(R.id.icono_error)).setImageResource(R.drawable.ic_message_error_24dp);

        final androidx.appcompat.app.AlertDialog alert=builder.create();

        v.findViewById(R.id.button_error).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        if(alert.getWindow() != null)
        {
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        alert.show();


    }

    public void ShowWarningDialog()
    {
        androidx.appcompat.app.AlertDialog.Builder builder= new androidx.appcompat.app.AlertDialog.Builder(getActivity(),R.style.AlertDialogTheme);
        v=LayoutInflater.from(getActivity()).inflate(R.layout.dialog_warning,(ConstraintLayout) v.findViewById(R.id.layoutDialogContainer) );

        builder.setView(v);
        ((TextView)v.findViewById(R.id.titulo_mensaje_warning)).setText("REVISE POR FAVOR");

        ((TextView)v.findViewById(R.id.detalle_mensaje_warning)).setText("Actualmente no tiene conexión a internet");

        ((Button)v.findViewById(R.id.button_warning)).setText("CONTINUAR");

        ((ImageView)v.findViewById(R.id.icono_warning)).setImageResource(R.drawable.ic_message_warning_24dp);

        final AlertDialog alert=builder.create();

        v.findViewById(R.id.button_warning).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                GetAllCupones();
            }
        });


        if(alert.getWindow() != null)
        {
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        alert.show();

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        v = inflater.inflate(R.layout.fragment_cupones, container, false);

        return v;
    }

    public static boolean isOnline(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    public void GetAllCupones() {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GetDataServiceCupones service = retrofit.create(GetDataServiceCupones.class);

        call = service.GetAllCupones();


        if (isOnline(getActivity())) {

            call.enqueue(new Callback<List<Cupon>>() {
                @Override
                public void onResponse(Call<List<Cupon>> call, Response<List<Cupon>> response) {

                    if (response.isSuccessful()) {
                        System.out.println(response.body());

                        List = response.body();

                        mAdapter = new CuponesAdapter(List, getActivity());

                        mRecyclerView.setAdapter(mAdapter);

                    } else {
                        switch (response.code()) {
                            case 404:
                                ShowErrorDialog();
                                break;
                            case 500:
                                ShowErrorDialog();
                                break;
                            default:
                                ShowErrorDialog();
                                break;
                        }
                    }

                }

                @Override
                public void onFailure(Call<List<Cupon>> call, Throwable t) {

                        ShowErrorDialog();
                }
            });

        }
        else
        {
            ShowWarningDialog();
        }

    }


    @Override
    public void doBack() {
      getFragmentManager().beginTransaction().replace(R.id.fragment_container,new SalonesFragment()).addToBackStack("a").commit();
    }
}

