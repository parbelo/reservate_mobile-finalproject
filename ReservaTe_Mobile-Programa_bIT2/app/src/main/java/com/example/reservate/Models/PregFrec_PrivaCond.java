package com.example.reservate.Models;

import com.google.gson.annotations.SerializedName;

public class PregFrec_PrivaCond {
    @SerializedName("id_ayuda")
    private int id_ayuda;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("descripcion")
    private String descripcion;


    public PregFrec_PrivaCond(int id_ayuda, String nombre, String descripcion) {
        this.id_ayuda = id_ayuda;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }


    public int getId_ayuda() {
        return id_ayuda;
    }

    public void setId_ayuda(int id_ayuda) {
        this.id_ayuda = id_ayuda;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


}


