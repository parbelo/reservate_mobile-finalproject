package com.example.reservate.Models;


import java.net.URL;

public class User {

    private Integer pk;
    private String username;
    private String password;
    private String email;
    private String first_name;
    private String last_name;
    private String FullName;

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public User(Integer pk, String username, String email, String fullName) {
        this.pk = pk;
        this.username = username;
        this.email = email;
        FullName = fullName;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(Integer pk, String username, String password, String email, String first_name, String last_name) {
        this.pk = pk;
        this.username = username;
        this.password = password;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
}
